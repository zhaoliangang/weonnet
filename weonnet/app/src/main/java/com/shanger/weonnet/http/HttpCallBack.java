package com.shanger.weonnet.http;



/**
 * ========================================
 * <p/>
 * 版 权：江苏精易达信息技术股份有限公司 版权所有 （C） 2018
 * <p/>
 * 作 者：liyunte
 * <p/>
 * <p/>
 * 版 本：1.0
 * <p/>
 * 创建日期：2018/3/12 11:23
 * <p/>
 * 描 述：网络回调接口
 * <p/>
 * <p/>
 * 修订历史：
 * <p/>
 * ========================================
 */
public interface HttpCallBack {
    /**}
     *
     * @param flag 与网络请求的入参flag对应。表述一个网络的请求与回调
     * @param message 服务器返回的内容
     */
    void onHttpSuccess(int flag, String message);
    /**
     *
     * @param flag 与网络请求的入参flag对应。表述一个网络的请求与回调
     * @param error_message 服务器返回的错误信息
     */
    void onHttpFail(int flag, String error_message);
}
