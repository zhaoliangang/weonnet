package com.shanger.weonnet.activity;

import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.shanger.weonnet.R;
import com.shanger.weonnet.base.BaseFragmentActivity;
import com.shanger.weonnet.fragment.ContactFragment;
import com.shanger.weonnet.fragment.MeFragment;
import com.shanger.weonnet.fragment.MessageFragment;
import com.shanger.weonnet.utils.ToastUtil;

import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends BaseFragmentActivity {
    @BindView(R.id.fl_main_container)
    FrameLayout flMainContainer;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.iv_main_tab_1)
    ImageView ivMainTab1;
    @BindView(R.id.tv_main_tab_1)
    TextView tvMainTab1;
    @BindView(R.id.ll_main_tab_1)
    LinearLayout llMainTab1;
    @BindView(R.id.tv_main_unread_news_count)
    TextView tvMainUnreadNewsCount;
    @BindView(R.id.iv_main_tab_2)
    ImageView ivMainTab2;
    @BindView(R.id.tv_main_tab_2)
    TextView tvMainTab2;
    @BindView(R.id.ll_main_tab_2)
    LinearLayout llMainTab2;
    @BindView(R.id.iv_main_tab_3)
    ImageView ivMainTab3;
    @BindView(R.id.tv_main_tab_3)
    TextView tvMainTab3;
    @BindView(R.id.ll_main_tab_3)
    LinearLayout llMainTab3;
    private boolean mIsExit;
    private MessageFragment messageFragment;
    private ContactFragment contactFragment;
    private MeFragment meFragment;
    private static final String TAG_MESSAGE = "tag_message_fragment";
    private static final String TAG_CONTACT = "tag_contact_fragment";
    private static final String TAG_ME = "tag_me_fragment";

    @Override
    protected int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void init() {
        initListener();
    }

    private void initListener() {
        setTabSelection(0);
    }

    private void setTabSelection(int index) {
        //重置按钮
        resetBtn();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        messageFragment = (MessageFragment) fragmentManager.findFragmentByTag(TAG_MESSAGE);
        contactFragment = (ContactFragment) fragmentManager.findFragmentByTag(TAG_CONTACT);
        meFragment = (MeFragment) fragmentManager.findFragmentByTag(TAG_ME);
//        fragment = (NewMoreFragment) fragmentManager.findFragmentByTag(TAG_MORE);
        if (messageFragment != null) {
            transaction.hide(messageFragment);
        }
        if (contactFragment != null) {
            transaction.hide(contactFragment);
        }
        if (meFragment != null) {
            transaction.hide(meFragment);
        }

        switch (index) {
            case 0:
                tvMainTab1.setTextColor(getResources().getColor(R.color.bule_light));
                ivMainTab1.setImageResource(R.mipmap.icon_btn_chats_s);
                if (messageFragment == null) {
                    messageFragment = new MessageFragment();
                    transaction.add(R.id.fl_main_container, messageFragment, TAG_MESSAGE);
                } else {
                    transaction.show(messageFragment);
                }
                break;
            case 1:
                tvMainTab2.setTextColor(getResources().getColor(R.color.bule_light));
                ivMainTab2.setImageResource(R.mipmap.icon_btn_contacts_s);

                if (contactFragment == null) {
                    contactFragment = new ContactFragment();
                    transaction.add(R.id.fl_main_container, contactFragment, TAG_CONTACT);
                } else {
                    transaction.show(contactFragment);
                }
                break;
            case 2:
                tvMainTab3.setTextColor(getResources().getColor(R.color.bule_light));
                ivMainTab3.setImageResource(R.mipmap.icon_btn_me_s);
                if (meFragment == null) {
                    meFragment = new MeFragment();
                    transaction.add(R.id.fl_main_container, meFragment, TAG_ME);
                } else {
                    transaction.show(meFragment);
                }
                break;
        }
        transaction.commit();
    }



    @Override
    protected void onSuccess(int flag, String message) {

    }

    @Override
    protected void onFailure(int flag, String error) {

    }

    @OnClick({R.id.ll_main_tab_1, R.id.ll_main_tab_2, R.id.ll_main_tab_3})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_main_tab_1:
                setTabSelection(0);
                break;
            case R.id.ll_main_tab_2:
                setTabSelection(1);
                break;
            case R.id.ll_main_tab_3:
                setTabSelection(2);
                break;
        }
    }

    /**
     * 清楚掉所有选中状态
     */
    private void resetBtn() {
        ivMainTab1.setImageResource(R.mipmap.icon_btn_chats_n);
        ivMainTab2.setImageResource(R.mipmap.icon_btn_contacts_n);
        ivMainTab3.setImageResource(R.mipmap.icon_btn_me_n);
        tvMainTab1.setTextColor(getResources().getColor(R.color.dark));
        tvMainTab2.setTextColor(getResources().getColor(R.color.dark));
        tvMainTab3.setTextColor(getResources().getColor(R.color.dark));
    }

    @Override
    /**
     * 双击返回键退出
     */
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mIsExit) {
                this.finish();
            } else {
                mIsExit = true;
                ToastUtil.show(R.string.exit);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mIsExit = false;
                    }
                }, 2000);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
