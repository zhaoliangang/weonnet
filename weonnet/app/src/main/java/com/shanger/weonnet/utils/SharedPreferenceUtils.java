/*
 * Created by liyunte on 17-7-20 下午3:18
 * Class Brief Introduction:
 *
 */

package com.shanger.weonnet.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by liyunte .
 * Use by SharedPreference工具类
 */
public class SharedPreferenceUtils {
    /**
     * 使用SharedPreference存储数据
     */
    public static void saveToSharedPreference(Context mContext, String key, String value){
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("vpro", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key,value);
        editor.commit();
    }

    /**
     * 使用SharedPreference得到数据
     */
    public static String getFromSharedPreference(Context mContext, String key){
        String string;
        try {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences("vpro", Context.MODE_PRIVATE);
            string = sharedPreferences.getString(key,"");
        }catch (Exception e){
            return "";
        }
        return string;
    }
}
