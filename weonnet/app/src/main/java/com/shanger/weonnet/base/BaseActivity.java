package com.shanger.weonnet.base;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.bumptech.glide.Glide;
import com.shanger.weonnet.http.HttpCallBack;
import com.shanger.weonnet.http.HttpManager;
import com.shanger.weonnet.utils.AppStatusManager;
import com.zhy.http.okhttp.OkHttpUtils;

import java.io.File;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import me.jessyan.autosize.AutoSize;

/**
 * ========================================
 * <p/>
 * 版 权：江苏精易达信息技术股份有限公司 版权所有 （C） 2018
 * <p/>
 * 作 者：liyunte
 * <p/>
 * <p/>
 * 版 本：1.0
 * <p/>
 * 创建日期：2018/3/12 11:23
 * <p/>
 * 描 述：Activity基类
 * <p/>
 * <p/>
 * 修订历史：
 * <p/>
 * ========================================
 */

public abstract class BaseActivity extends AppCompatActivity implements HttpCallBack {


    protected abstract int getLayout();//布局
    protected abstract void init();//初始化
    protected abstract void onSuccess(int flag, String message);//请求服务区成功
    protected abstract void onFailure(int flag, String error);//请求服务器失败或异常
    private Unbinder unbinder ;
    protected Activity mContext;
    private boolean isDestroy;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        mContext = this;
        AppStatusManager.getInstance().setAppStatus(AppStatusManager.AppStatusConstant.APP_NORMAL);
        onCreate();
        super.onCreate(savedInstanceState);
        AutoSize.autoConvertDensityOfGlobal(this);
        setStatusBar();
//        DensityHelper.resetDensity(mContext, 750f);
        setContentView(getLayout());
//        DensityHelper.resetDensity(mContext, 750f);
        unbinder = ButterKnife.bind(this);/*注解框架绑定*/
        init();
    }

    /**
     * 设置透明状态栏
     */
    private void setStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            if (isStatusBarWhite()) {
                window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            } else {
                window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            }
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(0);
        }
    }
    protected boolean isStatusBarWhite() {
        return false;
    }


    protected void onCreate(){

    }
    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
//        DensityHelper.resetDensity(mContext, 750f);
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onStop() {
        super.onStop();
        OkHttpUtils.getInstance().cancelTag(getName());//取消当前页面所有的网络请求
    }

    @Override
    protected void onDestroy() {
        isDestroy = true;
//        TmyApplication.getRefWatcher(this).watch(this);/*检查activity内存泄露*/
        super.onDestroy();
        if (unbinder!=null)unbinder.unbind();//注解框架解绑
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        if (level==TRIM_MEMORY_UI_HIDDEN){
            Glide.get(this).clearMemory();

        }
        Glide.get(this).trimMemory(level);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Glide.get(this).clearMemory();
//        System.gc();
//        System.runFinalization();
    }

    /**
     *
     * @return 实现类的简单名称
     */
    protected String getName(){
        return getClass().getSimpleName();
    }

    /**
     * 网络请求
     * @param url 请求地址
     * @param flag 区分是当前页面哪一个网络请求
     * @param parms 入参
     */
    protected void post(String url, final int flag, Map<String, Object> parms){
        HttpManager.post(mContext,getName(),url,flag,parms,this);
    }

    /**
     * 上传
     * @param url
     * @param flag
     * @param userid
     * @param headMap
     * @param fileMap
     */
     protected void up(String url, final  int flag, String userid, Map<String,Object> headMap, Map<String,File> fileMap, String model){
         HttpManager.up(mContext,getName(),url,flag,headMap,userid,fileMap,model,this);
     }
    /**
     * 请求成功的回调
     * @param flag 与网络请求的入参flag对应。表述一个网络的请求与回调
     * @param message 服务器返回的内容
     */
    @Override
    public void onHttpSuccess(int flag, String message) {
        if (!isDestroy){
            onSuccess(flag,message);
        }

    }

    /**
     * 请求失败或异常的回调
     * @param flag 与网络请求的入参flag对应。表述一个网络的请求与回调
     * @param error_message 服务器返回的错误信息
     */
    @Override
    public void onHttpFail(int flag, String error_message) {
        if (!isDestroy){
            onFailure(flag,error_message);
        }
    }
}
