package com.shanger.weonnet.http;


import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.shanger.weonnet.R;
import com.shanger.weonnet.utils.SharedPreferenceUtils;
import com.shanger.weonnet.utils.ToastUtil;
import com.zhy.http.okhttp.OkHttpUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;

import okhttp3.Call;
import okhttp3.MediaType;

/**
 * ========================================
 * <p/>
 * 版 权：江苏精易达信息技术股份有限公司 版权所有 （C） 2018
 * <p/>
 * 作 者：liyunte
 * <p/>
 * <p/>
 * 版 本：1.0
 * <p/>
 * 创建日期：2018/3/12 11:23
 * <p/>
 * 描 述：网络请求管理类
 * <p/>
 * <p/>
 * 修订历史：
 * <p/>
 * ========================================
 */
public class HttpManager {

    /**
     * 字符串换成UTF-8
     *
     * @param str
     * @return
     */
    public static String stringToUtf8(String str) {
      /*  String results = null;
        try {
           results = URLEncoder.encode(str, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }*/
        return str;
    }

    /**
     * utf-8换成字符串
     *
     * @param str
     * @return
     */
    public static String utf8ToString(String str) {
        String result = null;
        try {
            result = URLDecoder.decode(str, "utf-8");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;
    }


    /*
     * get请求
     * */
    public static void get(final Context context,String url,  Map<String, String> parms,final int flag,final HttpCallBack callBack) {
        String content = new JSONObject(parms).toString();
        Log.e("zlg", "url" + url);
        Log.e("zlg", "content" + content);
        OkHttpUtils.get()
                .url(url)
                .params(parms)
                .build()
                .execute(new com.zhy.http.okhttp.callback.StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        if (context != null) {
                            if (callBack != null) {
                                callBack.onHttpFail(flag, "网络异常,请检查网络");
                            }
                        }
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        if (context != null) {
                            if (callBack != null) {
                                try {
                                    String data = null;
                                    try {
                                        data = new String(response.getBytes(), "utf-8");
//                                        response = Emoji.decode(data);
                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    }
                                    JSONObject jsonObject = new JSONObject(response);
                                    if (jsonObject.getInt("code") == 200) {
                                        callBack.onHttpSuccess(flag, response);
                                    }else {
                                        ToastUtil.show(jsonObject.getString("msg"));
                                        Log.e("zlg","msg===="+jsonObject.getString("msg"));
                                        callBack.onHttpFail(flag, jsonObject.getInt("code") + "");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                });
    }


    public static void postPhp(final Context context,String url,  Map<String, String> parms,final int flag,final HttpCallBack callBack) {
        String content = new JSONObject(parms).toString();
        Log.e("zlg", "url" + url);
        Log.e("zlg", "content" + content);
        OkHttpUtils.post()
                .url(url)
                .params(parms)
                .build()
                .execute(new com.zhy.http.okhttp.callback.StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        Log.e("zlg","onError====="+e.getMessage()+"=====sssss======"+id);
                        if (context != null) {
                            if (callBack != null) {
                                callBack.onHttpFail(flag, "网络异常,请检查网络");
                            }
                        }
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        Log.e("zlg","onResponse");
                        if (context != null) {
                            if (callBack != null) {
                                try {
                                    String data = null;
                                    try {
                                        data = new String(response.getBytes(), "utf-8");
//                                        response = Emoji.decode(data);
                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    }
                                    JSONObject jsonObject = new JSONObject(response);
                                    if (jsonObject.getInt("code") == 200) {
                                        callBack.onHttpSuccess(flag, response);
                                    } else {
                                        ToastUtil.show(jsonObject.getString("msg"));
                                        Log.e("zlg","msg===="+jsonObject.getString("msg"));
                                        callBack.onHttpFail(flag, jsonObject.getInt("code") + "");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                });
    }

    /**
     * post请求
     *
     * @param url      请求链接
     * @param tag      请求的tag
     * @param parms    请求参数
     * @param flag     区分多个请求返回的数据
     * @param callBack 回调
     */
    public static void post(final Context context, String tag, String url, final int flag,
                            Map<String, Object> parms, final HttpCallBack callBack) {
        String content = new JSONObject(parms).toString();
//                 String content = Emoji.encode(new JSONObject(parms).toString());
        Log.e("liyunte", "url" + url);
        Log.e("liyunte", "content" + content);
        try {
            content = new String(content.getBytes("utf-8"), "ISO8859-1");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        OkHttpUtils.postString()
                .tag(tag)
                .url(url)
                .content(content)
                .mediaType(MediaType.parse("application/json; charset=utf-8"))
                .build()
                .execute(new com.zhy.http.okhttp.callback.StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        if (context != null) {
                            if (callBack != null) {
                                callBack.onHttpFail(flag, "网络异常,请检查网络");
                               /* if (flag!=1110){
                                    Toaster.showError(context,"网络不给力");
                                }*/
                            }
                        }


                    }

                    @Override
                    public void onResponse(String s, int id) {
                        if (context != null) {
                            if (callBack != null) {
                                try {
                                    String data = null;
                                    try {
                                        data = new String(s.getBytes(), "utf-8");
                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    }

                                    JSONObject jsonObject = new JSONObject(s);
                                    if (jsonObject.getInt("code") == 200) {
                                        callBack.onHttpSuccess(flag, s);
//                                        callBack.onHttpSuccess(flag,utf8ToString(s));
                                    } else {
                                        callBack.onHttpFail(flag, jsonObject.getString("err_msg") != null ?
                                                jsonObject.getString("err_msg") : "网络异常,请检查网络");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                });
    }




    public static void up(final Context context, final String tag, String url, final int flag,
                          Map<String, Object> headMap, String userid
            , Map<String, File> fileMap, String model, final HttpCallBack callBack) {
        JSONObject jsonObject = new JSONObject(headMap);
        Log.e("zlg",jsonObject.toString());
        OkHttpUtils.post().
                tag(tag)
                .url(url)
                .addParams("head", jsonObject.toString())
                .addParams("user_id", userid)
                .addParams("model", model)
                .files("files", fileMap)
                .build()
                .execute(new com.zhy.http.okhttp.callback.StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int i) {
                        callBack.onHttpFail(flag, "");
                    }

                    @Override
                    public void onResponse(String s, int i) {
                        if (context != null) {

                            if (callBack != null) {
                                try {
                                    JSONObject jsonObject = new JSONObject(s);
                                    if (jsonObject.getInt("status") == 0 && jsonObject.getInt("result_code") == 0) {
                                        callBack.onHttpSuccess(flag, s);
                                        Log.e("zlg",s);
                                    } else {

                                        callBack.onHttpFail(flag, "");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                });
    }
}
