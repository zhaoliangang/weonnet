package com.shanger.weonnet.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.shanger.weonnet.R;
import com.shanger.weonnet.adapter.MessageAdapter;
import com.shanger.weonnet.base.BaseFragment;
import com.shanger.weonnet.bean.MessageBean;
import com.zyyoona7.popup.EasyPopup;
import com.zyyoona7.popup.XGravity;
import com.zyyoona7.popup.YGravity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MessageFragment extends BaseFragment {
    @BindView(R.id.iv_more)
    ImageView ivMore;
    @BindView(R.id.rl_search)
    RelativeLayout rlSearch;
    @BindView(R.id.rv_message)
    RecyclerView rvMessage;
    @BindView(R.id.refresh)
    SmartRefreshLayout refresh;
    Unbinder unbinder;
    List<MessageBean> messageBeanList = new ArrayList<>();
    @BindView(R.id.rl_top)
    RelativeLayout rlTop;
    @BindView(R.id.iv_show)
    ImageView ivShow;
    private MessageAdapter adapter;
    private EasyPopup mCirclePop;

    @Override
    protected int getLayout() {
        return R.layout.fragment_message;
    }

    @Override
    protected void init() {
        initRecyclerView();
    }

    private void initRecyclerView() {
        adapter = new MessageAdapter(messageBeanList);
        LinearLayoutManager lm = new LinearLayoutManager(mContext);
        lm.setOrientation(LinearLayoutManager.VERTICAL);
        rvMessage.setLayoutManager(lm);
        rvMessage.setAdapter(adapter);
    }

    @Override
    protected void onSuccess(int flag, String message) {

    }

    @Override
    protected void onFailure(int flag, String error) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.iv_more, R.id.rl_search})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_more:
                showMore();
                break;
            case R.id.rl_search:
                break;
        }
    }

    private void showMore() {
        ivMore.setVisibility(View.GONE);
        ivShow.setVisibility(View.VISIBLE);
        mCirclePop = EasyPopup.create()
                .setContentView(mContext, R.layout.pop_more)
//                .setAnimationStyle(R.style.RightPopAnim)
                //是否允许点击PopupWindow之外的地方消失
                .setFocusAndOutsideEnable(true)
                .apply();
        mCirclePop.showAtAnchorView(ivShow, YGravity.BELOW, XGravity.RIGHT, 0, 0);
    }
}
