package com.shanger.weonnet.base;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shanger.weonnet.http.HttpCallBack;
import com.shanger.weonnet.http.HttpManager;
import com.zhy.http.okhttp.OkHttpUtils;

import java.io.File;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import me.jessyan.autosize.AutoSizeCompat;

/**
 * ========================================
 * <p/>
 * 版 权：江苏精易达信息技术股份有限公司 版权所有 （C） 2018
 * <p/>
 * 作 者：liyunte
 * <p/>
 * <p/>
 * 版 本：1.0
 * <p/>
 * 创建日期：2018/3/12 11:23
 * <p/>
 * 描 述：Fragment基类
 * <p/>
 * <p/>
 * 修订历史：
 * <p/>
 * ========================================
 */
public abstract class BaseFragment extends Fragment implements HttpCallBack {
    protected Activity mContext;//获取依赖的Activity对象
    protected abstract int getLayout();//布局
    protected abstract void init();//初始化
    protected abstract void onSuccess(int flag, String message);//请求服务区成功
    protected abstract void onFailure(int flag, String error);//请求服务器失败或异常
    private Unbinder unbinder ;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = (Activity)context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayout(),container,false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
    }

    @Override
    public void onStop() {
        super.onStop();
        OkHttpUtils.getInstance().cancelTag(getName());//取消网络请求
    }
private boolean isDestroy;
    @Override
    public void onDestroyView() {
        isDestroy = true;
        super.onDestroyView();
        if (unbinder!=null)unbinder.unbind();//注解框架解绑
    }



    @Override
    public void onInflate(Context context, AttributeSet attrs, Bundle savedInstanceState) {
//        DensityHelper.resetDensity(mContext, 750f);
        super.onInflate(context, attrs, savedInstanceState);
//        DensityHelper.resetDensity(mContext, 750f);
    }
    @NonNull
    @Override
    public LayoutInflater onGetLayoutInflater(@Nullable Bundle savedInstanceState) {
//        DensityHelper.resetDensity(mContext, 750f);
        return super.onGetLayoutInflater(savedInstanceState);
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
//        DensityHelper.resetDensity(mContext, 750f);
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
//        super.onSaveInstanceState(outState);
//        DensityHelper.resetDensity(mContext, 750f);
    }

    /**
     * post请求
     * @param url 网络url
     * @param flag 请求标志
     * @param parms 请求参数
     */
    protected void post(String url, final int flag,
                        Map<String, Object> parms){
        HttpManager.post(mContext,getName(),url,flag,parms,this);
    }

    /**
     * 上传
     * @param url
     * @param flag
     * @param userid
     * @param headMap
     * @param fileMap
     */
    protected void up(String url, final  int flag, String userid, Map<String,Object> headMap, Map<String,File> fileMap, String model){
        HttpManager.up(mContext,getName(),url,flag,headMap,userid,fileMap,model,this);
    }
    /**
     *
     * @return 获取子类名称
     */
    protected String getName(){
        return getClass().getSimpleName();
    }
    /**
     * 请求服务器成功的回调
     * @param flag 与网络请求的入参flag对应。表述一个网络的请求与回调
     * @param message 服务器返回的内容
     */
    @Override
    public void onHttpSuccess(int flag, String message) {
        if (!isDestroy){
            onSuccess(flag,message);
        }

    }
    /**
     * 请求服务器失败或异常的回调
     * @param flag 与网络请求的入参flag对应。表述一个网络的请求与回调
     * @param error_message 服务器返回的错误信息
     */
    @Override
    public void onHttpFail(int flag, String error_message) {
        if (!isDestroy){
            onFailure(flag,error_message);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
