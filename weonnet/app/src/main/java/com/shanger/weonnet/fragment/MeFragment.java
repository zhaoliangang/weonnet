package com.shanger.weonnet.fragment;

import com.shanger.weonnet.R;
import com.shanger.weonnet.base.BaseFragment;

public class MeFragment extends BaseFragment {
    @Override
    protected int getLayout() {
        return R.layout.fragment_me;
    }

    @Override
    protected void init() {

    }

    @Override
    protected void onSuccess(int flag, String message) {

    }

    @Override
    protected void onFailure(int flag, String error) {

    }
}
