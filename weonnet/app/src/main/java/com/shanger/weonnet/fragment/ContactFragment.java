package com.shanger.weonnet.fragment;

import com.shanger.weonnet.R;
import com.shanger.weonnet.base.BaseFragment;

public class ContactFragment extends BaseFragment {
    @Override
    protected int getLayout() {
        return R.layout.fragment_contact;
    }

    @Override
    protected void init() {

    }

    @Override
    protected void onSuccess(int flag, String message) {

    }

    @Override
    protected void onFailure(int flag, String error) {

    }
}
